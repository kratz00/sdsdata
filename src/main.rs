//
//      fetching data from the Sigma Docking Station TL2012
//

// --- configuration
//
const SDC_VENDOR: u16 = 0x1d9d; // TL2012 cradle id
const SDC_PRODUCT: u16 = 0x1011;

const BC0512: u8 = 0x09; // id of a BC  5.12 unit
const BC1612: u8 = 0x15; // id of a BC 16.12 unit

const TIMEOUT_SEND: u64 = 6000; // SDC is quite slow to reply...
const TIMEOUT_RECV: u64 = TIMEOUT_SEND * 2;
const TIMEOUT_POLL: u64 = 1000;

// --- crates and macros
//
#[macro_use]
extern crate clap;

extern crate libusb;

macro_rules! hexdump(         // hex dump on stderr
    ($info: expr, $arg: expr, $tot: expr) => { {
        eprint!("# ");
        for i in $arg.iter().take($tot) { eprint!(" {:02x}", i) }
        eprintln!("");
    } }
);

// --- sends an USB command code, expects a fixed size reply
//
fn command<'a>(
    handle: &mut libusb::DeviceHandle,
    code: u8,
    ret: &'a mut [u8],
    expected: usize,
) -> Result<&'a [u8], libusb::Error> {
    let send = libusb::request_type(
        libusb::Direction::Out,
        libusb::RequestType::Standard,
        libusb::Recipient::Endpoint,
    );
    let recv = libusb::request_type(
        libusb::Direction::In,
        libusb::RequestType::Standard,
        libusb::Recipient::Interface,
    );

    let write_timeout = std::time::Duration::from_millis(TIMEOUT_SEND);
    let read_timeout = std::time::Duration::from_millis(TIMEOUT_RECV);

    if code > 0 {
        let mut output_buf: [u8; 1] = [0];
        output_buf[0] = code;
        handle.write_bulk(send, &output_buf, write_timeout)?;
    } else {
        const RESET_BUF: [u8; 2] = [0xf0, 0x02]; // reset counters command
        handle.write_bulk(send, &RESET_BUF, write_timeout)?;
    }

    let bytes_read = handle.read_bulk(recv, &mut ret[..], read_timeout)?;
    if bytes_read == expected {
        Ok(&ret[..])
    } else {
        eprintln!(
            "# bulk transfer error: expected {} bytes, got {}",
            expected, bytes_read
        );
        Err(libusb::Error::NotSupported)
    }
}

fn main() -> Result<(), libusb::Error> {
    let flag = clap_app!( sdsdata =>
        (version: "2.0")
        (author: "Alfonso Martone <alfonso.martone@gmail.com>")
        (about: "Sigma SDS data extractor for TOPLINE 2012 BC x.x devices")
        (@arg clear:   --("clear")       "reset unit's counters after successful read")
        (@arg dump:    --("dump")        "dump hex data after successful reading info/data packets")
        (@arg miles:   --("miles")       "convert kilometers to miles")
        (@arg nots:    --("no-ts")       "do not output trip section ts_dist and ts_time values")
        (@arg nozeros: --("no-zeros")    "do not output zero-valued fields")
        (@arg raw:     --("raw")         "print comma-separated raw field values only")
        (@arg remove:  --("wait-remove") "after printing data wait for unit removal from the cradle")
        (@arg meters:  --("ts-dist")     +takes_value { |a| {
                                             let b = a.parse::<u32>();
                                             if b.is_ok() { Ok(()) } else { Err("not a non-negative integer".to_string()) } } }
                                         "adds offset distance to unit counters")
        (@arg seconds: --("ts-time")     +takes_value { |a| {
                                             let b = a.parse::<u32>();
                                             if b.is_ok() { Ok(()) } else { Err("not a non-negative integer".to_string()) } } }
                                         "adds offset time to unit counters")
        ).get_matches();

    let context = libusb::Context::new()?;
    let devices = context.devices()?;

    for mut device in devices.iter() {
        if let Ok(device_desc) = device.device_descriptor() {
            // don't bother checking if there is more than one cradle - first one, good one
            if device_desc.vendor_id() == SDC_VENDOR && device_desc.product_id() == SDC_PRODUCT {
                if let Ok(mut handle) = device.open() {
                    for n in 0..device_desc.num_configurations() {
                        let config_desc = match device.config_descriptor(n) {
                            Ok(c) => c,
                            Err(_) => continue,
                        };

                        for interface in config_desc.interfaces() {
                            for interface_desc in interface.descriptors() {
                                for endpoint_desc in interface_desc.endpoint_descriptors() {
                                    if endpoint_desc.direction() == libusb::Direction::In
                                        && endpoint_desc.transfer_type()
                                            == libusb::TransferType::Bulk
                                    {
                                        let config = config_desc.number();
                                        let interf_num = interface_desc.interface_number();
                                        let setting = interface_desc.setting_number();

                                        match handle.kernel_driver_active(interf_num) {
                                            Ok(true) => {
                                                handle.detach_kernel_driver(interf_num).ok()
                                            }
                                            _ => None,
                                        };

                                        handle.set_active_configuration(config)?;
                                        handle.claim_interface(interf_num)?;
                                        handle.set_alternate_setting(interf_num, setting)?;

                                        eprintln!(
                                            "cradle found on bus {:03} device {:03}",
                                            device.bus_number(),
                                            device.address()
                                        );

                                        return mappuoglio(flag, &mut handle);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    eprintln!(
        "could not get device {:04x}:{:04x} - \
         is it connected? do you need higher privileges?",
        SDC_VENDOR, SDC_PRODUCT
    );
    Err(libusb::Error::NoDevice)
}

// real meat comes here
//
fn mappuoglio(
    flag: clap::ArgMatches,
    handle: &mut libusb::DeviceHandle,
) -> Result<(), libusb::Error> {
    let short_timeout = std::time::Duration::from_millis(TIMEOUT_POLL);
    let mut input_buf: [u8; 32] = [0; 32]; // replies buffer
    let mut notify_cradle_still_empty = true;

    // check if the cradle channel needs some shaking
    if let Err(_) = command(handle, 0xf4, &mut input_buf[..], 1) {
        eprintln!("# resetting usb channel");
        handle.reset()?;
    }

    loop {
        // poll the cradle, expect a 0/1 value byte:
        command(handle, 0xf4, &mut input_buf[..], 1)?;
        if input_buf[0] == 1 {
            eprintln!("# unit found in the cradle");
            break;
        }

        if notify_cradle_still_empty {
            eprintln!("# cradle is empty; waiting...");
            notify_cradle_still_empty = false
        }

        std::thread::sleep(short_timeout)
    }

    // identify command, expecting 11 bytes:
    command(handle, 0xfe, &mut input_buf[..], 11)?;
    if flag.is_present("dump") {
        hexdump!("identification packet:", input_buf, 11)
    }

    // this release expects that input_buf bytes from 7 to 10 are always zero
    for i in 7..=10 {
        if input_buf[i] > 0 {
            eprintln!("# debug: unexpected values in bytes 7 to 10");
            break;
        }
    }

    match input_buf[1] {
        BC0512 => eprintln!(
            "# unit identified as a BC 5.12 type {} version {}",
            input_buf[0], input_buf[6]
        ),
        BC1612 => eprintln!(
            "# unit identified as a BC 16.12 type {} version {}",
            input_buf[0], input_buf[6]
        ),
        0 => {
            eprintln!("# unknown unit in the cradle (0), exiting...");
            return Err(libusb::Error::NotSupported);
        }
        _ => eprintln!(
            "# warning: unknown unit in the cradle (0x{:02x})",
            input_buf[1]
        ),
    }

    eprintln!(
        "# unit serial number: {}{}{}{}",
        input_buf[2], input_buf[3], input_buf[4], input_buf[5]
    );

    // fetch data command, expecting 27 bytes
    command(handle, 0xfb, &mut input_buf[..], 27)?;

    if flag.is_present("dump") {
        hexdump!("data packet:", input_buf, 27)
    }

    if flag.is_present("miles") {
        decode27(input_buf, &flag, "mi", "mph", Some(1.609344))
    } else {
        decode27(input_buf, &flag, "km", "km/h", None)
    }

    if flag.is_present("clear") {
        // reset unit's counters: a two bytes command, expecting a zero byte
        command(handle, 0, &mut input_buf[..], 1)?;
        if input_buf[0] != 0 {
            eprintln!(
                "# counters clearing probably failed (return code {})",
                input_buf[0]
            );
            return Ok(());
        }

        eprintln!("# non-ts counters cleared");
    }

    if flag.is_present("remove") {
        eprintln!("# waiting for unit removal from the cradle")
    }

    // pat the pet, or wait for the unit to be removed
    loop {
        command(handle, 0xf4, &mut input_buf[..], 1)?;

        if !flag.is_present("remove") {
            break;
        }
        if input_buf[0] == 0 {
            eprintln!("# unit removed, exiting...");
            break;
        }

        std::thread::sleep(short_timeout)
    }

    Ok(())
}

fn decode27(
    buf: [u8; 32],
    flag: &clap::ArgMatches,
    units_dist: &str,
    uspeed: &str,
    conv: Option<f64>,
) {
    let raw = flag.is_present("raw");
    let zeros = !flag.is_present("no-zeros");

    if buf[0] > 0 {
        eprintln!(
            "# {} records in the unit; reading the oldest one",
            buf[0] as usize + 1
        )
    }

    let mut dist = (buf[1] as u32) * 65536 + (buf[2] as u32) * 256 + (buf[3] as u32);
    let seconds = (buf[4] as u32) * 65536 + (buf[5] as u32) * 256 + (buf[6] as u32);
    let mut mean_sp = (buf[7] as u16) * 256 + (buf[8] as u16);
    let mut max_sp = (buf[9] as u16) * 256 + (buf[10] as u16);
    let cadence = buf[11]; // only set if cadence sensor was installed

    // this release expects that buf bytes 12 and 13 are always zero
    if buf[12] != 0 || buf[13] != 0 {
        eprintln!("# debug: unexpected values in bytes 12 and 13")
    }

    let mut ts_dist = (buf[14] as u32) * 65536 + (buf[15] as u32) * 256 + (buf[16] as u32);
    if flag.is_present("meters") {
        ts_dist = ts_dist + flag.value_of("meters").unwrap().parse::<u32>().unwrap()
    }

    let mut ts_seconds = (buf[17] as u32) * 65536 + (buf[18] as u32) * 256 + (buf[19] as u32);
    if flag.is_present("seconds") {
        ts_seconds = ts_seconds + flag.value_of("seconds").unwrap().parse::<u32>().unwrap()
    }

    // this release expects that buf bytes from 20 to 26 are always zero
    for i in 20..27 {
        if buf[i] > 0 {
            eprintln!("# debug: unexpected values in bytes 20 to 26");
            break;
        }
    }

    let hours = seconds / 3600;
    let minutes = seconds % 3600;
    let ts_hours = ts_seconds / 3600;
    let ts_mins = ts_seconds % 3600;

    if conv != None {
        let conv = conv.unwrap();
        dist = ((dist as f64) / conv) as u32;
        mean_sp = ((mean_sp as f64) / conv) as u16;
        max_sp = ((max_sp as f64) / conv) as u16;
        ts_dist = ((ts_dist as f64) / conv) as u32
    }

    if dist > 0 || zeros {
        if raw {
            print!("{}", dist)
        } else {
            println!(
                "distance: {}.{:02} {}",
                dist / 1000,
                (dist % 1000) / 10,
                units_dist
            )
        }
    }
    if raw {
        print!(",")
    }

    if seconds > 0 || zeros {
        if raw {
            print!("{}", seconds)
        } else {
            println!("time: {}:{:02}:{:02}", hours, minutes / 60, minutes % 60)
        }
    }
    if raw {
        print!(",")
    }

    if mean_sp > 0 || zeros {
        if raw {
            print!("{}.{:02}", mean_sp / 100, mean_sp % 100)
        } else {
            println!(
                "mean_speed: {}.{:02} {}",
                mean_sp / 100,
                mean_sp % 100,
                uspeed
            )
        }
    }
    if raw {
        print!(",")
    }

    if max_sp > 0 || zeros {
        if raw {
            print!("{}.{:02}", max_sp / 100, max_sp % 100)
        } else {
            println!("max_speed: {}.{:02} {}", max_sp / 100, max_sp % 100, uspeed)
        }
    }
    if raw {
        print!(",")
    }

    if cadence > 0 || zeros {
        if raw {
            print!("{}", cadence)
        } else {
            println!("cadence: {}/min", cadence)
        }
    }
    if raw {
        print!(",")
    }

    if !flag.is_present("no-ts") {
        if ts_dist > 0 || zeros {
            if raw {
                print!("{}", ts_dist)
            } else {
                println!(
                    "ts_dist: {}.{:02} {}",
                    ts_dist / 1000,
                    (ts_dist % 1000) / 10,
                    units_dist
                )
            }
        }
        if raw {
            print!(",")
        }

        if ts_seconds > 0 || zeros {
            if raw {
                print!("{}", ts_seconds)
            } else {
                println!(
                    "ts_time: {}:{:02}:{:02}",
                    ts_hours,
                    ts_mins / 60,
                    ts_mins % 60
                )
            }
        }
    }
    if raw {
        println!("")
    }
}
